/*

document.addEventListener('DOMContentLoaded', e => {

    for (const fileInput of document.querySelectorAll('input.preview-marker')) {
        const div = document.getElementById(fileInput.dataset.target);
        console.log(div);
        
        div.innerHTML = "Asdasdsa";
        fileInput.addEventListener('change', e => {
            
            var table =  document.createElement("TABLE");
            
            for(const image of e.target.files){
                var imageHtml =  document.createElement("IMG");
                imageHtml.src= window.URL.createObjectURL(image);
                var row = table.insertRow(-1);
                var cell = row.insertCell(-1);
                cell.appendChild(image);
            }
            div.appendChild(table);
        });
        
        const initialURL = fileInput.dataset.initial;
        if (initialURL) {
            img.src = initialURL;
        }
        
    }
});
*/
document.addEventListener('DOMContentLoaded', e => {
    for (const fileInput of document.querySelectorAll('input.preview-marker')) {
        const img = document.getElementById(fileInput.dataset.target);
        if (img) {
            fileInput.addEventListener('change', e => {
                img.src = window.URL.createObjectURL(e.target.files[0]);
            });
            const initialURL = fileInput.dataset.initial;
            if (initialURL) {
                img.src = initialURL;
            }
        }
    }
});
