from django.db import models
from django.utils.translation import gettext_lazy as _
# Create your models here.

def get_upload_path(instance, filename):
    disease = instance.disease.name
    return f'images/{disease}/{filename}'


class Disease(models.Model):
    name = models.CharField(_("Nombre"),max_length=150)
    description = models.CharField(_("Descipcion"),max_length=150)
    

class Feature(models.Model):
      # solo almacena la ruta al archivo
    image = models.ImageField(_('imágen'), upload_to=get_upload_path,null=True)
    disease = models.ForeignKey(Disease,on_delete=models.CASCADE,verbose_name=_("disease"),blank=False,null=False)
    features =  models.CharField(_("Caracteristicas"),max_length=4000)