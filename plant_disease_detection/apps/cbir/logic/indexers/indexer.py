
from apps.cbir.models import Feature

from ..descriptors.mangedescriptor import ManageDescriptor
from ..descriptors.colordescriptor import ColorDescriptor

import cv2
import numpy as np

class Indexer:
    def index_image(self, image):
        pass

class FileIndexer(Indexer):

    def __init__(self):
        pass
    
     #Hace el llamado a calcular el descriptor
    def index_image(self, feature):
        #Hacer geneérico esta paso
        mangedescriptor = ManageDescriptor(ColorDescriptor((8,12,3)))
        
        #Leer la imagen, feature.image es el path
        #image = cv2.imread(feature.image.url[1:])
        #Leer la imagen, feature.image de la memoria
        image = cv2.imdecode(np.fromstring(feature.image.read(), np.uint8), cv2.IMREAD_UNCHANGED)
        #Calcular las características de la imagen
        features = mangedescriptor.calculate_descriptor(image)
        
        #Pasar el desc a string
        desc = " ".join([str(x) for x in features])
        
        #se actualiza el modelo        

        feature.features = desc
