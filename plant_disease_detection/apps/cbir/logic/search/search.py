
from ..descriptors.mangedescriptor import ManageDescriptor
from ..descriptors.colordescriptor import ColorDescriptor

from ..comparers.chi2 import Chi2

from apps.cbir.models import Feature
# import the necessary packages
import numpy as np
import cv2


class Search:	
    def search(self):
        pass

class BasicSearch(Search):	

    def __init__(self, limit = 10):
        # store our index path
        self.limit = limit

    def search(self, in_memory_image):
        mangedescriptor = ManageDescriptor(ColorDescriptor((8,12,3)))
        
        #Leer la imagen
        image = cv2.imdecode(np.fromstring(in_memory_image.read(), np.uint8), cv2.IMREAD_UNCHANGED)
        #image = cv2.imread(query_path)

        #Calcular las características de la imagen
        queryFeatures = mangedescriptor.calculate_descriptor(image)

        #Comparar y traer las imagenes parecidas
        results = self.searchWithFeatures(queryFeatures, self.limit)

        return results
    def searchWithFeatures(self, queryFeatures, limit = 10):
        # initialize our dictionary of results
        results = []
        chi2=Chi2()
        
        #Consulta las imagenes  registradas
        images = Feature.objects.all()

        for i,image in enumerate(images):
            features = image.features
            features = [float(x) for x in features.split(" ")]
            d = chi2.match(features, queryFeatures, 1e-10)
            results.append((image,d))
        results.sort(reverse=True,key=lambda x: x[1])
        
        # return our (limited) results
        return [img for (img,_) in results[:limit]]