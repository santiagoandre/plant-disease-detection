
class ManageDescriptor:    
    def __init__(self, desc):
        self.descriptor = desc

    #Calcula el descriptor de la imagen, hace llamado al descriptor registrado.
    def calculate_descriptor(self, image):
        result_desc = None                
        result_desc = self.descriptor.describe(image)
        return result_desc
    