
from .comparer import Comparer
import numpy as np
import cv2
class Chi2(Comparer):
    def __init__(self):
        # store the number of bins for the 3D histogram		
        pass

    def match(self,features_1, features_2, eps):
        
        # compute the chi-squared distance
        d = 0.5 * np.sum([((a - b) ** 2) / (a + b + eps)
            for (a, b) in zip(features_1, features_2)])

        # return the chi-squared distance
        return d
        