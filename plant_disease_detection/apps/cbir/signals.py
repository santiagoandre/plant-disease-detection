from django.db.models.signals import m2m_changed, pre_save,post_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _
from  .logic.indexers.indexer import FileIndexer
from .models import Feature

indexer =  FileIndexer()
#presave in  inscription manytomany field
@receiver(pre_save, sender=Feature) 
def index_image(sender, instance, **kwargs):
    
    if(not instance.features):
        indexer.index_image(instance)
        print("Imangen indexadada " )
        #instance.save()
	