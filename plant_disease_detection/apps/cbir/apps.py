from django.apps import AppConfig


class CbirConfig(AppConfig):
    name = 'apps.cbir'
    def ready(self):
        import apps.cbir.signals