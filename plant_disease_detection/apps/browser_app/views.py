from django.shortcuts import render
from django.views.generic import  CreateView
from django.views.generic import  FormView
from .forms import BrowserForm
from apps.cbir.models import Feature
from apps.cbir.logic.search.search import BasicSearch
# Create your views here.


class BrowserView(FormView):
  form_class = BrowserForm
  template_name = 'browser/browser.html'
  success_url = '/browser'
  images = Feature.objects.all()  


def browserImages(image):
  s = BasicSearch()
  return s.search(image)

def browser(request, id=None):
    query_results = []
    query_img = None
    if request.method == "POST":
      query_img = request.FILES.getlist('browser_image')[0]
      query_results = browserImages(query_img)
    #print(query_results)
    form = BrowserForm()
  #  if query_img and query_img.url:
   #   query_img  = query_img.url[1:]
    print(query_img)
    print(dir(query_img))
    print(help(query_img))
    data = {
            'query_img': None,
            'query_results':query_results,
            'form': form
           }
    return render(request, 'browser/browser.html', data)