from django.apps import AppConfig


class BrowserAppConfig(AppConfig):
    name = 'apps.browser_app'
