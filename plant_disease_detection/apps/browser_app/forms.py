from django import forms

from .widgets import FileInputWithPreview

#help(forms.ClearableFileInput)
class BrowserForm(forms.Form):
  browser_image = forms.FileField(widget=FileInputWithPreview(attrs={'multiple': False,"accept":"image/*"}))
  def __init__(self,*args,**kwargs):
    super().__init__(*args,**kwargs)
