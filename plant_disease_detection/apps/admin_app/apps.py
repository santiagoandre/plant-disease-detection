from django.apps import AppConfig


class AdminAppConfig(AppConfig):
    name = 'apps.admin_app'
