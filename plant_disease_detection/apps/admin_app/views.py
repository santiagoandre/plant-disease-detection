from django.views import generic
from django.shortcuts import render
from apps.cbir.models import Feature, Disease
from .forms import DiseaseModelForm,ImagesForm
from django.views.generic import (ListView, DetailView, CreateView,
  UpdateView, FormView)
from apps.cbir.models import Feature

class DiseaseCreateView(CreateView):
  model = Disease
  form_class = DiseaseModelForm
  template_name = 'disease/new_disease.html'
  success_url = 'upload_images'
  # fields = '__all__'
  # fields = ['title', 'text', 'excerpt', 'author', 'related_image']
  # exclude = (...)

  def form_valid(self, form):
    return super().form_valid(form)


class UploadImagesView(FormView):
  form_class = ImagesForm
  template_name = 'disease/upload_images.html'
  success_url = 'browser'

  def form_valid(self, form):
    # procesar la info validada del formulario
    if form.is_valid():
      files = self.request.FILES.getlist('images')
      for image in files:
        Feature.objects.create(disease_id=form.cleaned_data["disease"],image=image)
      #form.save()
    return super().form_valid(form)
