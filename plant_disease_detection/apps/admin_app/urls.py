
from django.contrib import admin
from django.urls import path
from .views import DiseaseCreateView,UploadImagesView
urlpatterns = [
      path('create_disease', DiseaseCreateView.as_view(), name='disease.create'),
      path('upload_images', UploadImagesView.as_view(), name='disease.upload_images'),
      
]
