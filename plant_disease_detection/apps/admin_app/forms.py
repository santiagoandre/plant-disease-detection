from django import forms

from django.utils.translation import gettext_lazy as _

from apps.cbir.models import Feature, Disease
from django.utils.safestring import mark_safe


class AdminImageWidget(forms.FileInput):
    """
    A ImageField Widget for admin that shows a thumbnail.
    Taken from https://djangosnippets.org/snippets/1580/
    """

    template_name = 'file_with_preview_input.html'
    def __init__(self, attrs={}):
        super(AdminImageWidget, self).__init__(attrs)

    def render(self, name, value, *args,**kwargs):
        return super(AdminImageWidget, self).render(name, value, *args,**kwargs)
        
#help(forms.ClearableFileInput)
class ImagesForm(forms.Form):
  # <input type="text" minlength="5" maxlength="150" />
  #disease = forms.ModelChoiceField(queryset=Disease.objects.all(),widget=forms.Select(attrs={'class': 'form-control-file'})),
  disease = forms.ChoiceField(required=True, label=_('Enfermedad'))
  #image = forms.FileInput(attrs={'class': 'form-control-file'})
  images = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True,"accept":"image/*"} ),required=False)
  #images = forms.FileField(widget=FileInputWithPreview(attrs={'multiple': True,"accept":"image/*"}))
  def __init__(self,*args,**kwargs):
    super().__init__(*args,**kwargs)
    queryset=[(d.id,d.name) for d in Disease.objects.all()]
    self.fields['disease'].choices =  queryset

class DiseaseModelForm(forms.ModelForm):
  name = forms.CharField(min_length=2, max_length=150, label=_('Nombre'),
    widget=forms.TextInput(attrs={'class': 'form-control'}))
  description = forms.CharField(label=_('Descripcion'), widget=forms.Textarea(attrs={'class': 'form-control'}))
  

  class Meta:
    model = Disease
    fields = ('name', 'description')
    # exclude = (...)
    widgets = { # definición de atributos de inputs html
      'name': forms.Textarea(attrs={'class': 'form-control', 'rows': 2}),
      'description': forms.Textarea(attrs={'class': 'form-control'}),
    }

  def save(self, commit=True): # debe probarse
    # sobrescribe la lógica de registrar en BD del formulario
    # agregamos la lógica que necesitamos en mi funcionalidad
    # if self.cleaned_data['stars'] > 3:
    return super().save(commit=commit)

